package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreShouldBeIncreased() {
		//Arrange
		Player player = new Player ("Federer", 0);
		//Act
		player.incrementScore();
		//Assert
		assertEquals (1, player.getScore());
	}
	
	@Test
	public void scoreShouldNotBeIncreased() {
		//Arrange
		Player player = new Player ("Federer", 0);
		//Assert
		assertEquals (0, player.getScore());
	}
	
	@Test
	public void scoreShouldBeLove() {
		//Arrange
		Player player = new Player ("Federer", 0);
		//act
		String scoreAsString= player.getScoreAsString();
		//assert
		assertEquals ("love", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeFifteen() {
		//Arrange
		Player player = new Player ("Federer", 1);
		//act
		String scoreAsString= player.getScoreAsString();
		//assert
		assertEquals ("fifteen", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeThirty() {
		//Arrange
		Player player = new Player ("Federer", 2);
		//act
		String scoreAsString= player.getScoreAsString();
		//assert
		assertEquals ("thirty", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeForty() {
		//Arrange
		Player player = new Player ("Federer", 3);
		//act
		String scoreAsString= player.getScoreAsString();
		//assert
		assertEquals ("forty", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullIfNegative() {
		//Arrange
		Player player = new Player ("Federer", -1);
		//act
		String scoreAsString= player.getScoreAsString();
		//assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullIfMoreThanThree() {
		//Arrange
		Player player = new Player ("Federer", 4);
		//act
		String scoreAsString= player.getScoreAsString();
		//assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void scoreShouldBeTie() {
		//Arrange
		Player player1 = new Player ("Federer",1);
		Player player2 = new Player ("Nadal",1);
		//act
		boolean tie= player1.isTieWith(player2);
		//assert
		assertTrue (tie);
	}
	
	@Test
	public void scoreShouldNotBeTie() {
		//Arrange
		Player player1 = new Player ("Federer",1);
		Player player2 = new Player ("Nadal",2);
		//act
		boolean tie= player1.isTieWith(player2);
		//assert
		assertFalse (tie);
	}
	
	@Test
	public void scoreShouldHaveAtLeastFortyPoints () {
	//arrange
		Player player= new Player ("Nadal",3);
	//act
		boolean outcome= player.hasAtLeastFortyPoints();
	//assert
		assertTrue(outcome);
	}
	
	@Test
	public void scoreShouldNotHaveAtLeastFortyPoints () {
	//arrange
		Player player= new Player ("Nadal",2);
	//act
		boolean outcome= player.hasAtLeastFortyPoints();
	//assert
		assertFalse(outcome);
	}
	
	@Test
	public void scoreShouldHaveLessThanFortyPoints () {
	//arrange
		Player player= new Player ("Nadal",2);
	//act
		boolean outcome= player.hasLessThanFortyPoints();
	//assert
		assertTrue(outcome);
	}
	
	@Test
	public void scoreShouldNotHaveLessThanFortyPoints () {
	//arrange
		Player player= new Player ("Nadal",3);
	//act
		boolean outcome= player.hasLessThanFortyPoints();
	//assert
		assertFalse(outcome);
	}
	
	@Test
	public void scoreShouldHaveMoreThanFourtyPoints () {
	//arrange
		Player player= new Player ("Nadal",4);
	//act
		boolean outcome= player.hasMoreThanFourtyPoints();
	//assert
		assertTrue(outcome);
	}
	
	@Test
	public void scoreShouldNotHaveMoreThanFourtyPoints () {
	//arrange
		Player player= new Player ("Nadal",3);
	//act
		boolean outcome= player.hasMoreThanFourtyPoints();
	//assert
		assertFalse(outcome);
	}
	
	@Test
	public void player1HasOnePointAdvantageOnPlayer2 () {
	//arrange
		Player player1= new Player ("Nadal",4);
		Player player2= new Player ("Nadal",3);
	//act
	boolean point= player1.hasOnePointAdvantageOn(player2);
	//assert
		assertTrue(point);
	}
	
	@Test
	public void player1HasNotOnePointAdvantageOnPlayer2 () {
	//arrange
		Player player1= new Player ("Nadal",4);
		Player player2= new Player ("Nadal",4);
	//act
	boolean point= player1.hasOnePointAdvantageOn(player2);
	//assert
		assertFalse(point);
	}
	
	@Test
	public void scoreShouldhaveAtLeastTwoPointsAdvantageOn() {
	//arrange
		Player player1= new Player ("Nadal",4);
		Player player2= new Player ("Nadal",2);
	//act
	boolean point= player1.hasAtLeastTwoPointsAdvantageOn(player2);
	//assert
		assertTrue(point);
	}
	
	@Test
	public void scoreShouldNothaveAtLeastTwoPointsAdvantageOn() {
	//arrange
		Player player1= new Player ("Nadal",4);
		Player player2= new Player ("Nadal",3);
	//act
	boolean point= player1.hasAtLeastTwoPointsAdvantageOn(player2);
	//assert
		assertFalse(point);
	}
	
		
	
}


